pushd (dirname (status filename))
argparse d/dev -- $argv

if test -n "$_flag_dev"
    find Cargo.toml src | entr -r fish web.fish
else
    cargo install wasm-pack
    wasm-pack build -t web --release -d web/pkg
end
popd
