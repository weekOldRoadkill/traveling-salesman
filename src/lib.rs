// Modules
mod camera;
mod route;
mod time;
mod util;

// Namespaces
use crate::util::*;

// State struct
#[wasm_bindgen]
pub struct State {
    window: Window,
    canvas: HtmlCanvasElement,
    ctx: CanvasRenderingContext2d,
    camera: Camera,
    time: Time,
    route: Route,
    iters_per_sec: HtmlInputElement,
    point_count: HtmlInputElement,
    variation: HtmlInputElement,
    variation_disp: Element,
    dist_smooth: HtmlInputElement,
    dist_smooth_disp: Element,
}

// State implementation
#[wasm_bindgen]
impl State {
    // Create a new state
    #[wasm_bindgen(constructor)]
    pub fn new() -> Result<State, JsError> {
        Self::inner_new().unwrap_js()
    }
    fn inner_new() -> Result<State> {
        let window = window().ok_or_eyre("Failed to get window")?;
        let doc = window.document().ok_or_eyre("Failed to get document")?;
        let canvas = doc
            .get_element_by_id("canvas")
            .ok_or_eyre("Failed to get canvas")?
            .dyn_into::<HtmlCanvasElement>()
            .wrap_debug()?;
        let ctx = canvas
            .get_context("2d")
            .wrap_debug()?
            .ok_or_eyre("Failed to get 2d context")?
            .dyn_into()
            .wrap_debug()?;
        let fps_disp = doc
            .get_element_by_id("fps_disp")
            .ok_or_eyre("Failed to get fps_disp")?;
        let avg_dist_disp = doc
            .get_element_by_id("avg_dist_disp")
            .ok_or_eyre("Failed to get avg_dist_disp")?;
        let iters_per_sec = doc
            .get_element_by_id("iters_per_sec")
            .ok_or_eyre("Failed to get iters_per_sec")?
            .dyn_into()
            .wrap_debug()?;
        let point_count = doc
            .get_element_by_id("point_count")
            .ok_or_eyre("Failed to get point_count")?
            .dyn_into()
            .wrap_debug()?;
        let variation = doc
            .get_element_by_id("variation")
            .ok_or_eyre("Failed to get variation")?
            .dyn_into()
            .wrap_debug()?;
        let variation_disp = doc
            .get_element_by_id("variation_disp")
            .ok_or_eyre("Failed to get variation_disp")?;
        let dist_smooth = doc
            .get_element_by_id("dist_smooth")
            .ok_or_eyre("Failed to get dist_smooth")?
            .dyn_into()
            .wrap_debug()?;
        let dist_smooth_disp = doc
            .get_element_by_id("dist_smooth_disp")
            .ok_or_eyre("Failed to get dist_smooth_disp")?;
        doc.get_element_by_id("version_disp")
            .ok_or_eyre("Failed to get version_disp")?
            .set_inner_html(env!("CARGO_PKG_VERSION"));
        let mut state = State {
            window,
            canvas,
            ctx,
            camera: Camera::default(),
            time: Time::new(fps_disp),
            route: Route::new(|_| DVec2::ZERO, 0, avg_dist_disp),
            iters_per_sec,
            point_count,
            variation,
            variation_disp,
            dist_smooth,
            dist_smooth_disp,
        };
        state.inner_resize()?;
        Ok(state)
    }

    // Resize the canvas
    pub fn resize(&mut self) -> Result<(), JsError> {
        self.inner_resize().unwrap_js()
    }
    fn inner_resize(self: &mut Self) -> Result<()> {
        let res = self.window.device_pixel_ratio()
            * dvec2(
                self.window
                    .inner_width()
                    .wrap_debug()?
                    .as_f64()
                    .ok_or_eyre("Failed to get window width")?,
                self.window
                    .inner_height()
                    .wrap_debug()?
                    .as_f64()
                    .ok_or_eyre("Failed to get window height")?,
            );
        self.canvas.set_width(res.x as _);
        self.canvas.set_height(res.y as _);
        self.camera = Camera::new(res);
        self.camera.apply(&self.ctx)?;
        Ok(())
    }

    // Tick the state
    pub fn tick(&mut self, time: f64) -> Result<(), JsError> {
        self.inner_tick(0.001 * time).unwrap_js()
    }
    fn inner_tick(self: &mut Self, time: f64) -> Result<()> {
        self.time.tick(time);

        let point_count = self.point_count.value_as_number().max(1.0) as usize;
        if point_count != self.route.len() {
            self.route = Route::new(
                |_| Self::rand_point(),
                point_count,
                self.route.avg_dist_disp.clone(),
            );
        }

        let iters_per_sec = self.iters_per_sec.value_as_number().max(0.0);
        let variation = self.variation.value_as_number();
        self.variation_disp
            .set_inner_html(&format!("{:.0}%", 100.0 * variation));
        let mut dist_smooth = self.dist_smooth.value_as_number();
        self.dist_smooth_disp
            .set_inner_html(&format!("{:.0}%", 100.0 * dist_smooth));
        dist_smooth = 1.0 - dist_smooth;
        for _ in 0..((self.time.delta * iters_per_sec) as usize).min(100) {
            self.route.score(variation, dist_smooth);
        }

        self.camera.clear(&self.ctx);
        self.route.render(&self.ctx, &self.time)?;
        Ok(())
    }

    // Random point
    fn rand_point() -> DVec2 {
        (0.5 * Math::random() + 0.5) * DVec2::from_angle(f64::consts::TAU * Math::random())
    }
}
