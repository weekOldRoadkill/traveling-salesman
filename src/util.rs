// Namespaces
pub(crate) use crate::{camera::*, route::*, time::*};
pub(crate) use eyre::*;
pub(crate) use glam::{f64::*, FloatExt};
pub(crate) use std::{collections::*, *};
pub(crate) use web_sys::{js_sys::*, wasm_bindgen::prelude::*, *};

// Wrap debug trait
pub(crate) trait WrapDebug<T> {
    // Wrap result with debug display to eyre result
    fn wrap_debug(self: Self) -> Result<T>;
}

// Wrap debug implementation
impl<T, D> WrapDebug<T> for Result<T, D>
where
    D: fmt::Debug,
{
    // Wrap result with debug display to eyre result
    fn wrap_debug(self: Self) -> Result<T> {
        self.map_err(|err| eyre!("{err:?}"))
    }
}

// Unwrap JS trait
pub(crate) trait UnwrapJs<T> {
    // Unwrap eyre result to JS result
    fn unwrap_js(self: Self) -> Result<T, JsError>;
}

// Unwrap JS implementation
impl<T> UnwrapJs<T> for Result<T> {
    // Unwrap eyre result to JS result
    fn unwrap_js(self: Self) -> Result<T, JsError> {
        self.map_err(|err| JsError::new(&format!("{err}")))
    }
}

// Float comparison trait
pub(crate) trait FloatCmp {
    // Compare two floats
    fn float_cmp(self: &Self, other: &Self) -> cmp::Ordering;
}

// Float comparison implementation
impl<T> FloatCmp for T
where
    T: PartialOrd,
{
    // Compare two floats
    fn float_cmp(self: &Self, other: &Self) -> cmp::Ordering {
        self.partial_cmp(other).unwrap_or(cmp::Ordering::Equal)
    }
}

// Weighted random trait
pub(crate) trait WeightedRand<T> {
    // Weighted random
    fn weighted_rand(self: Self, by: impl Fn(&T) -> f64) -> Option<T>;
}

// Weighted random implementation
impl<T, I> WeightedRand<T> for I
where
    I: iter::Iterator<Item = T>,
{
    // Weighted random
    fn weighted_rand(self: Self, by: impl Fn(&T) -> f64) -> Option<T> {
        let dist: Vec<_> = self
            .scan(0.0, |acc, item| {
                *acc += by(&item).max(f64::MIN_POSITIVE);
                Some((*acc, item))
            })
            .collect();
        let r = dist.last()?.0 * Math::random();
        dist.into_iter().find(|(w, _)| r < *w).map(|(_, item)| item)
    }
}
