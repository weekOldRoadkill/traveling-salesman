// Namespaces
use crate::util::*;

// Camera struct
#[derive(Default)]
pub(crate) struct Camera {
    res: DVec2,
    trans: DAffine2,
    inv_trans: DAffine2,
}

// Camera implementation
impl Camera {
    // Create a new camera
    pub(crate) fn new(res: DVec2) -> Self {
        let half_res = 0.5 * res;
        let trans = DAffine2::from_scale_angle_translation(
            DVec2::splat(half_res.min_element()),
            0.0,
            half_res,
        );
        Self {
            res,
            trans,
            inv_trans: trans.inverse(),
        }
    }

    // Apply the camera
    pub(crate) fn apply(self: &Self, ctx: &CanvasRenderingContext2d) -> Result<()> {
        ctx.set_transform(
            self.trans.matrix2.x_axis.x,
            self.trans.matrix2.x_axis.y,
            self.trans.matrix2.y_axis.x,
            self.trans.matrix2.y_axis.y,
            self.trans.translation.x,
            self.trans.translation.y,
        )
        .wrap_debug()?;
        Ok(())
    }

    // Clear the camera
    pub(crate) fn clear(self: &Self, ctx: &CanvasRenderingContext2d) -> () {
        let pos = self.inv_trans.transform_point2(DVec2::ZERO);
        let size = self.inv_trans.transform_vector2(self.res);
        ctx.clear_rect(pos.x, pos.y, size.x, size.y);
        ctx.begin_path();
        ctx.close_path();
    }
}
