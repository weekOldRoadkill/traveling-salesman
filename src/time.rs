// Namespaces
use crate::util::*;

// Time struct
pub(crate) struct Time {
    pub(crate) time: f64,
    pub(crate) delta: f64,
    fps_disp: Element,
    fps_timer: f64,
}

// Time implementation
impl Time {
    // Create a new time struct
    pub(crate) fn new(fps_disp: Element) -> Self {
        Self {
            time: 0.0,
            delta: 0.0,
            fps_disp,
            fps_timer: 0.0,
        }
    }

    // Tick the time
    pub(crate) fn tick(self: &mut Self, time: f64) -> () {
        self.delta = time - self.time;
        self.time = time;
        if self.time > self.fps_timer {
            self.fps_disp
                .set_inner_html(&format!("{:.0}", 1.0 / self.delta));
            self.fps_timer = self.time + 0.25;
        }
    }
}
