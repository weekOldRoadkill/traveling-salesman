// Namespaces
use crate::util::*;

// Point struct
#[derive(Clone)]
pub(crate) struct Point {
    pos: DVec2,
    weights: BTreeMap<usize, f64>,
}

// Route struct
pub(crate) struct Route {
    points: Vec<Point>,
    avg_dist: f64,
    pub(crate) avg_dist_disp: Element,
    avg_dist_timer: f64,
}

// Route implementation
impl Route {
    // Constants
    const RADIUS: f64 = 0.01;
    const WIDTH: f64 = 0.005;

    // Create a new route
    pub(crate) fn new(f: impl Fn(usize) -> DVec2, size: usize, avg_dist_disp: Element) -> Self {
        Self {
            points: (0..size)
                .into_iter()
                .map(|i| Point {
                    pos: f(i),
                    weights: (0..size)
                        .into_iter()
                        .filter_map(|j| if i == j { None } else { Some((j, 0.0)) })
                        .collect(),
                })
                .collect(),
            avg_dist: 0.0,
            avg_dist_disp,
            avg_dist_timer: 0.0,
        }
    }

    // Render the route
    pub(crate) fn render(
        self: &mut Self,
        ctx: &CanvasRenderingContext2d,
        time: &Time,
    ) -> Result<()> {
        ctx.set_stroke_style_str("#FE8019C0");
        ctx.set_line_width(Self::WIDTH);
        ctx.set_line_join("round");
        ctx.begin_path();
        let mut points = self.points.clone();
        let mut i = 0;
        ctx.move_to(points[i].pos.x, points[i].pos.y);
        while let Some((&j, _)) = points[i].weights.iter().max_by(|a, b| a.1.float_cmp(b.1)) {
            for point in &mut points {
                point.weights.remove(&i);
            }
            ctx.line_to(points[j].pos.x, points[j].pos.y);
            i = j;
        }
        ctx.close_path();
        ctx.stroke();

        ctx.set_fill_style_str("#EBDBB2");
        ctx.begin_path();
        for point in &self.points {
            ctx.move_to(point.pos.x, point.pos.y);
            ctx.ellipse(
                point.pos.x,
                point.pos.y,
                Self::RADIUS,
                Self::RADIUS,
                0.0,
                0.0,
                f64::consts::TAU,
            )
            .wrap_debug()?;
        }
        ctx.close_path();
        ctx.fill();

        if time.time > self.avg_dist_timer {
            self.avg_dist_disp
                .set_inner_html(&format!("{:.2}", self.avg_dist));
            self.avg_dist_timer = time.time + 0.25;
        }
        Ok(())
    }

    // Run an ant through the route and get the distance traveled and the path taken
    fn run_ant(self: &Self, variation: f64) -> (f64, Vec<usize>) {
        let mut points = self.points.clone();
        let mut dist = 0.0;
        let mut path = Vec::with_capacity(points.len());
        let mut i = (points.len() as f64 * Math::random()) as usize;
        path.push(i);
        while let Some(j) = if Math::random() > variation {
            points[i]
                .weights
                .iter()
                .weighted_rand(|(_, w)| **w)
                .map(|(j, _)| *j)
        } else {
            points[i]
                .weights
                .iter()
                .map(|(j, w)| (j, w, points[i].pos.distance_squared(points[*j].pos)))
                .min_by(|(_, _, a), (_, _, b)| a.float_cmp(b))
                .map(|(j, _, _)| *j)
        } {
            for point in &mut points {
                point.weights.remove(&i);
            }
            dist += points[i].pos.distance(points[j].pos);
            i = j;
            path.push(i);
        }
        (dist, path)
    }

    // Score the route by running ants through it
    pub(crate) fn score(self: &mut Self, variation: f64, dist_smooth: f64) -> () {
        let (dist, path) = self.run_ant(variation);
        let mut score = self.avg_dist / dist - 1.0;
        if score.is_sign_negative() {
            score *= 1.5;
        }
        self.avg_dist = self.avg_dist.lerp(dist, dist_smooth);
        for indices in path.windows(2) {
            if let Some(w) = self.points[indices[0]].weights.get_mut(&indices[1]) {
                *w = (*w + score).max(0.0);
            }
        }
    }
}

// Route deref points implementation
impl ops::Deref for Route {
    // Target type
    type Target = Vec<Point>;

    // Deref points
    fn deref(self: &Self) -> &Self::Target {
        &self.points
    }
}
